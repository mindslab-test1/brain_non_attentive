import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import pytorch_lightning as pl
import random
import numpy as np
from omegaconf import OmegaConf

from datasets.text import Language
from modules import TextEncoder, TacotronDecoder, Audio2Mel
from datasets import TextMelDataset, text_mel_collate
from utils.alignment_loss import GuidedAttentionLoss


class Tacotron(pl.LightningModule):
    def __init__(self, hparams, hp=None):
        super().__init__()
        self.hparams = hparams  # used for pl
        if hp is None:
            hp = OmegaConf.load(hparams.config)
        self.hp = hp
        self.symbols = Language(hp.data.lang, hp.data.text_cleaners).get_symbols()
        self.symbols = ['"{}"'.format(symbol) for symbol in self.symbols]
        self.encoder = TextEncoder(hp.chn.encoder, hp.ker.encoder, hp.depth.encoder, len(self.symbols))
        self.speaker_embedding = nn.Embedding(len(hp.data.speakers), hp.chn.speaker)
        self.teacher = TacotronDecoder(hp)
        self.audio2mel = Audio2Mel(hp.audio.filter_length, hp.audio.hop_length, hp.audio.win_length,
                                   hp.audio.sampling_rate, hp.audio.n_mel_channels, hp.audio.mel_fmin,
                                   hp.audio.mel_fmax)
        self.warm_start = False
        self.is_val_first = True
        self.attn_loss = GuidedAttentionLoss(20000, 0.25, 1.00025)

        self.collate_fn = text_mel_collate()

    def forward(self, text, mel_target, duration_target, speakers, input_lengths, output_lengths, max_input_len,
                prenet_dropout=0.5, no_mask=False, tfrate=1.0):
        text_encoding = self.encoder(text, input_lengths)  # [B, N, chn.encoder]
        speaker_emb = self.speaker_embedding(speakers)  # [B, chn.speaker]
        speaker_emb = speaker_emb.unsqueeze(1).expand(-1, text_encoding.size(1), -1)  # [B, N, chn.speaker]
        decoder_input = torch.cat((text_encoding, speaker_emb), dim=2)
        mel_pred, mel_postnet, alignment, duration = \
            self.teacher(mel_target, text_encoding, decoder_input, duration_target, input_lengths, output_lengths,
                         max_input_len, prenet_dropout, no_mask, tfrate)
        return mel_pred, mel_postnet, alignment, duration

    def inference(self, text, speakers, max_decoder_steps, mel_chunk_size, prenet_dropout=0.5, attn_reset=False, pace=1.0):
        text_encoding = self.encoder.inference(text)
        speaker_emb = self.speaker_embedding(speakers)
        speaker_emb = speaker_emb.unsqueeze(1).expand(-1, text_encoding.size(1), -1)
        if text_encoding.dtype == torch.float16:
            speaker_emb = speaker_emb.half()
        decoder_input = torch.cat((text_encoding, speaker_emb), dim=2)
        return self.teacher.inference(
            decoder_input, prenet_dropout, attn_reset, max_decoder_steps, mel_chunk_size, pace)

    def training_step(self, batch, batch_idx):
        text, mel_target, duration_target, speakers, input_lengths, output_lengths, max_input_len, _ = batch
        mel_pred, mel_postnet, alignment, duration = \
            self(text, mel_target, duration_target, speakers, input_lengths, output_lengths, max_input_len,
                         prenet_dropout=0.5, tfrate=self.hp.train.teacher_force.rate)

        spec_loss = F.mse_loss(mel_pred, mel_target) + F.mse_loss(mel_postnet, mel_target) + \
                    F.l1_loss(mel_pred, mel_target) + F.l1_loss(mel_postnet, mel_target)

        duration_loss = F.mse_loss(duration, duration_target / (self.hp.audio.sampling_rate / self.hp.audio.hop_length))

        #alignment_loss = self.attn_loss(alignment, input_lengths, output_lengths, self.global_step)

        loss = spec_loss + self.hp.train.loss_rate.dur * duration_loss

        self.logger.log_loss(spec_loss, mode='train', step=self.global_step, name='MSE+L1')
        self.logger.log_loss(duration_loss, mode='train', step=self.global_step, name='duration')
        #self.logger.log_loss(alignment_loss, mode='train', step=self.global_step, name='Guided')
        self.logger.log_loss(loss, mode='train', step=self.global_step)

        return {'loss': loss}

    def validation_step(self, batch, batch_idx):
        text, mel_target, duration_target, speakers, input_lengths, output_lengths, max_input_len, _ = batch

        mel_pred, mel_postnet, alignment, duration = \
            self(text, mel_target, duration_target, speakers, input_lengths, output_lengths, max_input_len,
                         prenet_dropout=0.5, tfrate=1.0)

        loss = F.mse_loss(mel_pred, mel_target) + F.mse_loss(mel_postnet, mel_target) + \
               F.l1_loss(mel_pred, mel_target) + F.l1_loss(mel_postnet, mel_target)

        if self.is_val_first:  # plot alignment, character embedding
            self.is_val_first = False
            self.logger.log_figures(mel_pred, mel_postnet, mel_target, alignment, self.global_step)
            self.logger.log_embedding(self.symbols, self.encoder.embedding.weight, self.global_step)

        return {'loss': loss}

    def validation_end(self, outputs):
        loss = torch.stack([x['loss'] for x in outputs]).mean()
        self.logger.log_loss(loss, mode='val', step=self.global_step)
        self.is_val_first = True
        return {'val_loss': loss}

    def configure_optimizers(self):
        if self.warm_start:
            learnable_params = self.speaker_embedding.parameters()
        else:
            learnable_params = self.parameters()
        return torch.optim.Adam(
            learnable_params,
            lr=self.hp.train.adam.lr,
            weight_decay=self.hp.train.adam.weight_decay,
        )

    def lr_lambda(self, step):
        progress = (step - self.hp.train.decay.start) / (self.hp.train.decay.end - self.hp.train.decay.start)
        return self.hp.train.decay.rate ** np.clip(progress, 0.0, 1.0)

    def optimizer_step(self, epoch_nb, batch_nb, optimizer, optimizer_idx, second_order_closure):
        lr_scale = self.lr_lambda(self.global_step)
        for pg in optimizer.param_groups:
            pg['lr'] = lr_scale * self.hp.train.adam.lr

        optimizer.step()
        optimizer.zero_grad()

        self.logger.log_learning_rate(lr_scale * self.hp.train.adam.lr, self.global_step)

    def train_dataloader(self):
        trainset = TextMelDataset(self.hp, self.hp.data.train_dir, self.hp.data.train_meta, self.hp.data.rawdata_dir, train=True)
        return DataLoader(trainset, batch_size=self.hp.train.batch_size, shuffle=True,
                          num_workers=self.hp.train.num_workers,
                          collate_fn=self.collate_fn, pin_memory=True, drop_last=False)

    def val_dataloader(self):
        valset = TextMelDataset(self.hp, self.hp.data.val_dir, self.hp.data.val_meta, self.hp.data.rawdata_dir, train=False)
        return DataLoader(valset, batch_size=self.hp.train.batch_size, shuffle=False,
                          num_workers=self.hp.train.num_workers,
                          collate_fn=self.collate_fn, pin_memory=False, drop_last=False)

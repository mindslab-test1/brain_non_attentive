import os
import re
import torch
import random
import librosa
import numpy as np
import time
from torch.utils.data import Dataset
from collections import Counter

from .text import Language
from .text.cmudict import CMUDict
from modules.mel import Audio2Mel


class TextMelDataset(Dataset):
    def __init__(self, hp, data_dir, metadata_path, rawdata_dir, train=True):
        super().__init__()
        self.hp = hp
        self.lang = Language(hp.data.lang, hp.data.text_cleaners, use_eos=True)
        self.train = train
        self.data_dir = data_dir
        metadata_path = os.path.join(data_dir, metadata_path)
        self.meta = self.load_metadata(metadata_path)
        self.rawdata_dir = rawdata_dir
        self.speaker_dict = {speaker: idx for idx, speaker in enumerate(hp.data.speakers)}

        if train:
            # balanced sampling for each speaker
            speaker_counter = Counter((spk_id \
                                       for basename, spk_id, text, raw_text in self.meta))
            weights = [1.0 / speaker_counter[spk_id] \
                       for basename, spk_id, text, raw_text in self.meta]

            self.mapping_weights = torch.DoubleTensor(weights)

        self.audio2mel = Audio2Mel(
            filter_length=hp.audio.filter_length, hop_length=hp.audio.hop_length,
            win_length=hp.audio.win_length, sampling_rate=hp.audio.sampling_rate,
            n_mel_channels=hp.audio.n_mel_channels,
            mel_fmin=hp.audio.mel_fmin, mel_fmax=hp.audio.mel_fmax)

        if hp.data.lang == 'eng2':
            self.cmudict = CMUDict(hp.data.cmudict_path)
            self.cmu_pattern = re.compile(r'^(?P<word>[^!\'(),-.:~?]+)(?P<punc>[!\'(),-.:~?]+)$')

    def __len__(self):
        return len(self.meta)

    def __getitem__(self, idx):
        if self.train:
            idx = torch.multinomial(self.mapping_weights, 1).item()

        basename, spk_id, text, raw_text = self.meta[idx]
        melpath = os.path.join(self.data_dir, 'mel', '{}-mel-{}.npy'.format(spk_id, basename))
        mel = self.get_mel(melpath)
        text_norm = self.get_text(text)
        new_path = os.path.join(self.rawdata_dir, spk_id, '{}.wav.predict'.format(basename))
        dur_path = os.path.join(self.data_dir, 'duration', '{}-duration-{}.npy'.format(spk_id, basename))
        duration = np.load(dur_path)
        duration = torch.from_numpy(duration)
        spk_id = self.speaker_dict[spk_id]

        return text_norm, mel, duration, spk_id, new_path

    def get_mel(self, melpath):
        mel = np.load(melpath)
        mel = torch.from_numpy(mel.T)
        assert mel.size(0) == self.hp.audio.n_mel_channels, \
            'Mel dimension mismatch: expected %d, got %d' % \
            (self.hp.audio.n_mel_channels, mel.size(0))
        return mel

    def get_text(self, text):
        # if lang='eng2', then use representation mixing. (arXiv:1811.07240)
        # i.e., randomly apply CMUDict-based English g2p to whole sentence.
        # note that lang='eng2' will use arpabet WITH stress.
        if self.hp.data.lang == 'eng2' and random.random() < 0.5:
            text = ' '.join([self.get_arpabet(word) for word in text.split(' ')])
        text_norm = torch.LongTensor(self.lang.text_to_sequence(text, self.hp.data.text_cleaners))
        return text_norm

    def get_arpabet(self, word):
        arpabet = self.cmudict.lookup(word)
        if arpabet is None:
            match = self.cmu_pattern.search(word)
            if match is None:
                return word
            subword = match.group('word')
            arpabet = self.cmudict.lookup(subword)
            if arpabet is None:
                return word
            punc = match.group('punc')
            arpabet = '{%s}%s' % (arpabet[0], punc)
        else:
            arpabet = '{%s}' % arpabet[0]

        if random.random() < 0.5:
            return word
        else:
            return arpabet

    def load_metadata(self, path, split="|"):
        metadata = []
        with open(path, 'r', encoding='utf-8') as f:
            for line in f:
                stripped = line.strip().split(split)
                if self.hp.train.fine_tuning and stripped[2] != self.hp.train.tuning_speaker:
                    continue
                metadata.append(stripped)

        return metadata


class text_mel_collate():
    def __call__(self, batch):
        input_lengths, ids_sorted_decreasing = torch.sort(
            torch.LongTensor([len(x[0]) for x in batch]),
            dim=0, descending=True)
        max_input_len = torch.empty(len(batch), dtype=torch.long)
        max_input_len.fill_(input_lengths[0])

        text_padded = torch.zeros((len(batch), max_input_len[0]), dtype=torch.long)
        n_mel_channels = batch[0][1].size(0)
        max_target_len = max([x[1].size(1) for x in batch])

        mel_padded = torch.zeros(len(batch), n_mel_channels, max_target_len)
        output_lengths = torch.empty(len(batch), dtype=torch.long)
        speakers = torch.empty(len(batch), dtype=torch.long)

        duration_padded = torch.zeros((len(batch), max_input_len[0]), dtype=torch.long)

        new_paths = []

        for idx, key in enumerate(ids_sorted_decreasing):
            text = batch[key][0]
            text_padded[idx, :text.size(0)] = text
            mel = batch[key][1]
            mel_padded[idx, :, :mel.size(1)] = mel
            duration = batch[key][2]
            duration_padded[idx, :duration.size(0)] = duration
            output_lengths[idx] = mel.size(1)
            speakers[idx] = batch[key][3]
            new_paths.append(batch[key][4])

        return text_padded, mel_padded, duration_padded, speakers, \
               input_lengths, output_lengths, max_input_len, new_paths

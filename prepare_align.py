import os
import argparse

import yaml
import librosa
import numpy as np
from scipy.io import wavfile
from tqdm import tqdm


def main(config):
    in_dir = config["path"]["corpus_path"]
    out_dir = config["path"]["raw_path"]
    train_meta = config["path"]["train_meta"]
    val_meta = config["path"]["val_meta"]
    sampling_rate = config["preprocessing"]["audio"]["sampling_rate"]
    max_wav_value = config["preprocessing"]["audio"]["max_wav_value"]

    cv = 0
    for dataset in [train_meta, val_meta]:
        with open(os.path.join(in_dir, dataset), encoding="utf-8") as f:
            for line in tqdm(f):
                wav_name, text, speaker = line.strip("\n").split("|")
                wav_path = os.path.join(in_dir, wav_name)

                if os.path.exists(wav_path):
                    wav, _ = librosa.load(wav_path, sampling_rate)
                    wav = wav / max(abs(wav)) * max_wav_value
                    wav_name = wav_name.strip("\n").split("/")
                    wav_name = wav_name[-1]
                    if cv == 0:
                        os.makedirs(os.path.join(out_dir, 'train', speaker), exist_ok=True)
                        wavfile.write(
                            os.path.join(out_dir, 'train', speaker, wav_name),
                        sampling_rate,
                            wav.astype(np.int16),
                        )
                        with open(
                            os.path.join(out_dir, 'train', speaker, "{}.lab".format(wav_name[:-4])),
                            "w",
                        ) as f1:
                            f1.write(text)
                    elif cv == 1:
                        os.makedirs(os.path.join(out_dir, 'val', speaker), exist_ok=True)
                        wavfile.write(
                            os.path.join(out_dir, 'val', speaker, wav_name),
                            sampling_rate,
                            wav.astype(np.int16),
                        )
                        with open(
                                os.path.join(out_dir, 'val', speaker, "{}.lab".format(wav_name[:-4])),
                                "w",
                        ) as f1:
                            f1.write(text)
        cv += 1

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, help="path to preprocess.yaml")
    args = parser.parse_args()

    config = yaml.load(open(args.config, "r"), Loader=yaml.FullLoader)
    main(config)
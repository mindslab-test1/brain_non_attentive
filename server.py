import os
import grpc
import time
import yaml
import torch
import logging
import argparse
import threading
import numpy as np

from concurrent import futures

from datasets.text import Language
from tacotron import Tacotron

import tts_pb2
from tts_pb2_grpc import add_AcousticServicer_to_server, AcousticServicer

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class TacotronImpl(AcousticServicer):
    def __init__(self, checkpoint_path, device, max_decoder_steps, mel_chunk_size, model_config_path, is_fp16):
        super().__init__()
        self.is_fp16 = is_fp16
        self.max_decoder_steps = max_decoder_steps
        self.mel_chunk_size = mel_chunk_size
        self.model_status = tts_pb2.ModelStatus()

        torch.cuda.set_device(device)
        self.device = device
        try:
            self._load_checkpoint(checkpoint_path, model_config_path)
        except Exception as e:
            self.model_status.state = tts_pb2.MODEL_STATE_NONE
            self.model_status.model.Clear()
            logging.exception(e)

    @torch.no_grad()
    def Txt2Mel(self, in_text, context):
        try:
            torch.cuda.set_device(self.device)
            mel_generator = self._inference(in_text, context, self.max_decoder_steps)
            if mel_generator is None:
                return tts_pb2.MelSpectrogram()

            mel_outputs_postnet = [mel for mel in mel_generator][0]
            sample_length = mel_outputs_postnet.size(-1) * self.mel_config.hop_length
            mel_outputs_postnet = mel_outputs_postnet.reshape(-1).cpu().tolist()

            mel = tts_pb2.MelSpectrogram(data=mel_outputs_postnet,
                                         sample_length=sample_length)
            return mel

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    @torch.no_grad()
    def StreamTxt2Mel(self, in_text, context):
        try:
            torch.cuda.set_device(self.device)
            mel_generator = self._inference(in_text, context, self.mel_chunk_size)
            if mel_generator is not None:
                for mel_outputs_postnet in mel_generator:
                    mel_outputs_postnet = mel_outputs_postnet.reshape(-1).cpu().tolist()

                    mel = tts_pb2.MelSpectrogram(data=mel_outputs_postnet)
                    yield mel

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def GetMelConfig(self, empty, context):
        if self.model_status.state == tts_pb2.MODEL_STATE_RUNNING:
            return self.mel_config
        else:
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
            return tts_pb2.MelConfig()

    def SetModel(self, in_model, context, is_running=False):
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details("model changing method not implemented yet")
        logging.exception("model changing method not implemented yet")
        raise NotImplementedError

    def GetModel(self, empty, context):
        return self.model_status

    def _inference(self, in_text, context, mel_chunk_size):
        if self.model_status.state != tts_pb2.MODEL_STATE_RUNNING:
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
            return None
        if self.max_speaker < in_text.speaker:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details("exceeded maxspeaker")
            return None

        text = in_text.text
        text = self.lang.text_to_sequence(text, self.hp.data.text_cleaners)
        text = torch.LongTensor(text).unsqueeze(0).cuda()

        speaker = in_text.speaker
        speaker = torch.LongTensor([speaker]).cuda()

        return self.model.inference(text, speaker, self.max_decoder_steps, mel_chunk_size)

    def _gen_hparams(self, config_paths):
        # generate hparams object for pl.LightningModule
        parser = argparse.ArgumentParser()
        parser.add_argument('--config')
        args = parser.parse_args(['--config', config_paths])
        return args

    def _load_checkpoint(self, checkpoint_path, model_config_path):
        self.model_status.state = tts_pb2.MODEL_STATE_LOADING

        args = self._gen_hparams(model_config_path)
        self.model = Tacotron(args).cuda()
        self.hp = self.model.hp
        self.lang = Language(self.hp.data.lang, self.hp.data.text_cleaners)

        checkpoint = torch.load(checkpoint_path, map_location='cpu')
        self.model.load_state_dict(checkpoint['state_dict'])
        self.model.eval()
        self.model.freeze()
        del checkpoint
        torch.cuda.empty_cache()

        if self.is_fp16:
            self.model.teacher.fvae.attention.score_mask_value = np.finfo('float16').min
            from apex import amp
            self.model.encoder, _ = amp.initialize(self.model.encoder, [], opt_level="O3")
            self.model.speaker_embedding, _ = amp.initialize(self.model.speaker_embedding, [], opt_level="O3")
            self.model.teacher, _ = amp.initialize(self.model.teacher, [], opt_level="O3")

        if self.hp.audio.mel_fmax is None:
            self.hp.audio.mel_fmax = self.model.hp.audio.mel_fmax = \
                self.hp.audio.sampling_rate // 2
        self.mel_config = tts_pb2.MelConfig(
            filter_length=self.hp.audio.filter_length,
            hop_length=self.hp.audio.hop_length,
            win_length=self.hp.audio.win_length,
            n_mel_channels=self.hp.audio.n_mel_channels,
            sampling_rate=self.hp.audio.sampling_rate,
            mel_fmin=self.hp.audio.mel_fmin,
            mel_fmax=self.hp.audio.mel_fmax,
        )
        self.max_speaker = len(self.hp.data.speakers) - 1

        self.model_status.state = tts_pb2.MODEL_STATE_RUNNING


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='tacotron(DCA) runner executor')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model Path.',
                        type=str)
    parser.add_argument('-l', '--log_level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=30001)
    parser.add_argument('-d', '--device',
                        nargs='?',
                        dest='device',
                        help='gpu device',
                        type=int,
                        default=0)
    parser.add_argument('-s', '--max_decoder_steps',
                        nargs='?',
                        dest='max_decoder_steps',
                        help='voice length limit: (max decoder steps * hop_length / sampling_rate) second',
                        type=int,
                        default=30000)
    parser.add_argument('-w', '--max_workers',
                        nargs='?',
                        dest='max_workers',
                        help='max workers',
                        type=int,
                        default=4)
    parser.add_argument('--mel_chunk_size',
                        nargs='?',
                        dest='mel_chunk_size',
                        help='mel chunk size',
                        type=int,
                        default=88)
    parser.add_argument('--is_fp16', action='store_true',
                        help='fp16 mode')
    parser.add_argument('-c', '--config',
                        nargs='?',
                        dest='config',
                        help='yaml file for configuration',
                        type=str,
                        default="")

    args = parser.parse_args()
    tacotron = TacotronImpl(
        args.model, args.device, args.max_decoder_steps, args.mel_chunk_size, args.config, args.is_fp16)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.max_workers), )
    add_AcousticServicer_to_server(tacotron, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('tacotron(DCA) starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
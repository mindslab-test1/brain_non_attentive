import random
import math
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from .upsampling import GetDuration, GetRange, Upsampling


class PreNet(nn.Module):
    def __init__(self, hp, channels, in_dim, depth):
        super().__init__()
        sizes = [in_dim] + [channels] * depth
        self.hp = hp
        self.layers = nn.ModuleList(
            [nn.Linear(in_size, out_size)
                for (in_size, out_size) in zip(sizes[:-1], sizes[1:])])

    # in default tacotron2 setting, we use prenet_dropout=0.5 for both train/infer.
    # If the prenet_dropout is not 0.5, inference does not work well.
    def forward(self, x, prenet_dropout):
        for linear in self.layers:
            x = F.dropout(F.relu(linear(x)), p=prenet_dropout, training=True)
        return x


class PostNet(nn.Module):
    def __init__(self, channels, kernel_size, n_mel_channels, depth):
        super().__init__()
        padding = (kernel_size - 1) // 2
        self.cnn = list()
        self.cnn.append(
            nn.Sequential(
                nn.Conv1d(n_mel_channels, channels, kernel_size=kernel_size, padding=padding),
                nn.BatchNorm1d(channels),
                nn.Tanh(),
                nn.Dropout(0.5), ))

        for i in range(1, depth - 1):
            self.cnn.append(
                nn.Sequential(
                    nn.Conv1d(channels, channels, kernel_size=kernel_size, padding=padding),
                    nn.BatchNorm1d(channels),
                    nn.Tanh(),
                    nn.Dropout(0.5), ))

        self.cnn.append(
            nn.Sequential(
                nn.Conv1d(channels, n_mel_channels, kernel_size=kernel_size, padding=padding), ))

        self.cnn = nn.Sequential(*self.cnn)

    def forward(self, x):
        return self.cnn(x)


class PositionalEncoding(nn.Module):
    """This class implements the positional encoding function.
    PE(pos, 2i)   = sin(pos/(10000^(2i/dmodel)))
    PE(pos, 2i+1) = cos(pos/(10000^(2i/dmodel)))
    Arguments
    ---------
    max_len : int
        Max length of the input sequences (default 2500).
    Example
    -------
    >>> a = torch.rand((8, 120, 512))
    >>> enc = PositionalEncoding(input_size=a.shape[-1])
    >>> b = enc(a)
    >>> b.shape
    torch.Size([8, 120, 512])
    """

    def __init__(self, input_size, max_len=2500):
        super().__init__()
        self.max_len = max_len
        pe = torch.zeros(self.max_len, input_size, requires_grad=False)
        positions = torch.arange(0, self.max_len).unsqueeze(1).float()
        denominator = torch.exp(
            torch.arange(0, input_size, 2).float()
            * -(math.log(10000.0) / input_size)
        )

        pe[:, 0::2] = torch.sin(positions * denominator)
        pe[:, 1::2] = torch.cos(positions * denominator)

        self.register_buffer("pe", pe)

    def forward(self, T):
        """
        Arguments
        ---------
        x : tensor
            Input feature shape (batch, time, fea)
        """
        return self.pe[: T].clone().detach()


class TacotronDecoder(nn.Module):
    def __init__(self, hp):
        super().__init__()
        self.hp = hp
        self.go_frame = nn.Parameter(
            torch.randn(1, hp.audio.n_mel_channels), requires_grad=True)
        self.n_mel_channels = hp.audio.n_mel_channels
        self.prenet = PreNet(
            hp, hp.chn.prenet, in_dim=hp.audio.n_mel_channels, depth=hp.depth.encoder)
        self.postnet = PostNet(
            hp.chn.postnet, hp.ker.postnet, hp.audio.n_mel_channels, hp.depth.postnet)
        self.get_duration = GetDuration(hp.chn.encoder + hp.chn.speaker, hp.chn.dur_lstm)
        self.get_range = GetRange(hp.chn.encoder + hp.chn.speaker + 1, hp.chn.range_lstm)
        self.upsampling_layer = Upsampling()
        self.get_pos_emb = PositionalEncoding(hp.chn.pos_dim)
        self.upsampled_rnn_1 = nn.LSTMCell(hp.chn.prenet + hp.chn.encoder + hp.chn.speaker + hp.chn.pos_dim,
                                           hp.chn.decoder_rnn)
        self.upsampled_rnn_2 = nn.LSTMCell(hp.chn.decoder_rnn, hp.chn.decoder_rnn)
        self.mel_fc = nn.Linear(
            hp.chn.decoder_rnn + hp.chn.encoder + hp.chn.speaker + hp.chn.pos_dim, hp.audio.n_mel_channels)

    def get_go_frame(self, memory):
        return self.go_frame.expand(memory.size(0), self.hp.audio.n_mel_channels)

    def initialize(self, memory, mask):
        self.memory = memory
        self.mask = mask

    def decode(self, x, upsampled):
        x = torch.cat((x, upsampled), dim=-1)
        # [B, chn.prenet + (chn.encoder + chn.speaker) + chn.pos_dim]
        x, _ = self.upsampled_rnn_1(x)
        x = F.dropout(x, 0.1, self.training)
        x, _ = self.upsampled_rnn_2(x)
        # [B, chn.decoder_rnn]
        x = F.dropout(x, 0.1, self.training)

        x = torch.cat((x, upsampled), dim=-1)
        # [B, chn.decoder_rnn + (chn.encoder + chn.speaker) + chn.pos_dim]
        mel_out = self.mel_fc(x)
        # [B, audio.n_mel_channels]

        return mel_out

    def parse_decoder_inputs(self, decoder_inputs):
        """ Prepares decoder inputs, i.e. mel outputs
        PARAMS
        ------
        decoder_inputs: inputs used for teacher-forced training, i.e. mel-specs
        RETURNS
        -------
        inputs: processed decoder inputs
        """
        # (B, n_mel_channels, T_out) -> (B, T_out, n_mel_channels)
        decoder_inputs = decoder_inputs.transpose(1, 2)
        # (B, T_out, n_mel_channels) -> (T_out, B, n_mel_channels)
        decoder_inputs = decoder_inputs.transpose(0, 1)
        return decoder_inputs

    def parse_decoder_outputs(self, mel_outputs, alignments):
        # 'T' is T_dec.
        mel_outputs = torch.stack(mel_outputs, dim=0).transpose(0, 1).contiguous()
        mel_outputs = mel_outputs.view(
            mel_outputs.size(0), -1, self.n_mel_channels)
        mel_outputs = mel_outputs.transpose(1, 2)
        # mel: [T, B, M] -> [B, T, M] -> [B, M, T]
        if alignments is None:
            return mel_outputs

        alignments = torch.stack(alignments, dim=0).transpose(0, 1).contiguous()
        # align: [T_dec, B, T_enc] -> [B, T_dec, T_enc]

        return mel_outputs, alignments

    def forward(self, x, text_encoding, memory, target_duration, memory_lengths, output_lengths, max_input_len,
                prenet_dropout=0.5, no_mask=False, tfrate=1.0):
        # x: mel spectrogram for teacher-forcing. [B, M, T].
        mel_spec = x
        go_frame = self.get_go_frame(memory).unsqueeze(0)
        x = self.parse_decoder_inputs(x)  # [B, M, T] -> [B, T, M] -> [T, B, M]
        x = torch.cat((go_frame, x), dim=0)  # [T+1, B, M]
        x = self.prenet(x, prenet_dropout)

        self.initialize(memory, mask=None if no_mask else ~self.get_mask_from_lengths(memory_lengths))

        duration_s = self.get_duration(memory, self.mask, memory_lengths) # [B, N]
        # duration = torch.round(duration_s * (self.hp.audio.sampling_rate / self.hp.audio.hop_length))
        # duration.data.masked_fill_(self.mask, 0.0)
        sigma = self.get_range(memory, target_duration, self.mask, memory_lengths)  # [B, N]
        # sigma = torch.ones_like(target_duration, dtype=duration_s.dtype, device=target_duration.device) * 10
        # sigma.data.masked_fill_(self.mask, 1e-8)

        center = torch.cumsum(target_duration, dim=-1) - target_duration // 2
        gaussian = torch.distributions.normal.Normal(loc=center, scale=sigma)

        pe = self.get_pos_emb(mel_spec.size(2) + 1)  # [T, chn.pos_dim]
        index_list = []
        target_duration = target_duration.long()
        for b in range(target_duration.size(0)):
            index_list_1 = []
            for n in range(target_duration.size(1)):
                if target_duration[b, n] > 0:
                    index_list_1.append(torch.arange(1, target_duration[b, n].detach() + 1))
            if len(index_list_1) == 0:
                index_1 = torch.zeros(mel_spec.size(2), dtype=torch.long)
            else:
                index_1 = torch.cat(index_list_1, dim=0)  # [T]
                index_1 = F.pad(index_1, (0, mel_spec.size(2) - index_1.size(0)))
            index_list.append(index_1)
        index = torch.stack(index_list, dim=0)  # [B, T]

        alignments, mel_outputs = [], []
        decoder_input = x[0]

        while len(mel_outputs) < x.size(0) - 1:
            upsampled, prev_attn = self.upsampling_layer(self.memory, gaussian, len(mel_outputs), self.mask)
            # upsampled: [B, (chn.encoder + chn.speaker)], prev_attn: [B, N]

            pos_emb = []
            for i in range(index.size(0)):
                if index[i, len(mel_outputs)] == 0:
                    pos_emb.append(torch.zeros_like(pe[0]))
                else:
                    pos_emb.append(pe[index[i, len(mel_outputs)]])
            pos_emb = torch.stack(pos_emb, dim=0)  # [B, chn.pos_dim]

            upsampled = torch.cat((upsampled, pos_emb), dim=-1)  # [B, (chn.encoder + chn.speaker) + chn.pos_dim]

            mel_out = \
                self.decode(decoder_input, upsampled)

            mel_outputs.append(mel_out)
            alignments.append(prev_attn)

            if tfrate < random.random():
                decoder_input = self.prenet(mel_out, prenet_dropout)
            else:
                decoder_input = x[len(mel_outputs)]

        mel_outputs, alignments = self.parse_decoder_outputs(mel_outputs, alignments)
        mel_postnet = mel_outputs + self.postnet(mel_outputs)

        # DataParallel expects equal sized inputs/outputs, hence padding
        alignments = alignments.unsqueeze(0)
        alignments = F.pad(alignments, (0, max_input_len[0] - alignments.size(-1)), 'constant', 0)
        alignments = alignments.squeeze(0)

        mel_outputs, mel_postnet, alignments = \
            self.mask_output(mel_outputs, mel_postnet, alignments, output_lengths)
        return mel_outputs, mel_postnet, alignments, duration_s

    def inference(self, memory, prenet_dropout, attn_reset, max_decoder_steps, mel_chunk_size, pace):
        decoder_input = self.get_go_frame(memory)
        self.initialize(memory, mask=None)
        mel_outputs, alignments = [], []

        duration_s = self.get_duration.inference(memory) # [B, N]
        duration_s = duration_s * pace
        duration = torch.round(duration_s * (self.hp.audio.sampling_rate / self.hp.audio.hop_length))
        sigma = self.get_range.inference(memory, duration)  # [B, N]
        # sigma = torch.ones_like(duration_s, dtype=duration_s.dtype, device=duration_s.device) * 10

        center = torch.cumsum(duration, dim=-1) - duration / 2
        gaussian = torch.distributions.normal.Normal(loc=center, scale=sigma)

        output_len = int(torch.sum(duration, dim=-1).detach())
        pe = self.get_pos_emb(output_len + 1)  # [T, chn.pos_dim]
        index_list = []
        duration = duration.long()
        for b in range(duration.size(0)):
            index_list_1 = []
            for n in range(duration.size(1)):
                if duration[b, n] > 0:
                    index_list_1.append(torch.arange(1, duration[b, n].detach() + 1))
            if len(index_list_1) == 0:
                index_1 = torch.zeros(output_len, dtype=torch.long)
            else:
                index_1 = torch.cat(index_list_1, dim=0)  # [T]
            index_list.append(index_1)
        index = torch.stack(index_list, dim=0)  # [B, T]

        while len(mel_outputs) < output_len:
            upsampled, prev_attn = self.upsampling_layer(self.memory, gaussian, len(mel_outputs), self.mask)
            # upsampled: [B, (chn.encoder + chn.speaker)], prev_attn: [B, N]

            if attn_reset and torch.max(prev_attn) < 0.4:
                idx = torch.argmax(prev_attn)
                prev_attn = torch.zeros_like(prev_attn)
                prev_attn[:, idx] = 1.0

            pos_emb = []
            for i in range(index.size(0)):
                if index[i, len(mel_outputs)] == 0:
                    pos_emb.append(torch.zeros_like(pe[0]))
                else:
                    pos_emb.append(pe[index[i, len(mel_outputs)]])
            pos_emb = torch.stack(pos_emb, dim=0)  # [B, chn.pos_dim]

            upsampled = torch.cat((upsampled, pos_emb), dim=-1)  # [B, (chn.encoder + chn.speaker) + chn.pos_dim]

            decoder_input = self.prenet(decoder_input, prenet_dropout)

            mel_out = \
                self.decode(decoder_input, upsampled)

            mel_outputs.append(mel_out)
            alignments.append(prev_attn)

            decoder_input = mel_out

            # if len(mel_outputs) == mel_chunk_size:
            #     yield self.flush_mel_outputs(mel_outputs)
            #     mel_outputs.clear()

            if len(mel_outputs) == max_decoder_steps:
                print("Warning! Reached max decoder steps")
                break
        alignments = torch.stack(alignments, dim=0).transpose(0, 1).contiguous()
        if 0 < len(mel_outputs):
            return self.flush_mel_outputs(mel_outputs), alignments

        # mel_outputs, alignments = self.parse_decoder_outputs(mel_outputs, alignments)
        # mel_postnet = mel_outputs + self.postnet(mel_outputs)
        # return mel_outputs, mel_postnet, alignments
    
    def flush_mel_outputs(self, mel_outputs):
        mel_outputs = self.parse_decoder_outputs(mel_outputs, None)
        mel_postnet = mel_outputs + self.postnet(mel_outputs)
        return mel_postnet

    def get_mask_from_lengths(self, lengths, max_len=None):
        if max_len is None:
            max_len = torch.max(lengths).item()
        ids = torch.arange(0, max_len, out=torch.cuda.LongTensor(max_len))
        mask = (ids < lengths.unsqueeze(1))
        return mask

    def mask_output(self, mel_outputs, mel_postnet, alignments, output_lengths=None):
        if self.hp.train.mask_padding and output_lengths is not None:
            mask = ~self.get_mask_from_lengths(output_lengths, max_len=mel_outputs.size(-1))
            mask = mask.unsqueeze(1)  # [B, 1, T] torch.bool
            mel_outputs.masked_fill_(mask, 0.0)
            mel_postnet.masked_fill_(mask, 0.0)

        return mel_outputs, mel_postnet, alignments

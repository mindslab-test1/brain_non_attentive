import math
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from scipy.special import gamma


class Attention(nn.Module):
    def __init__(self, hidden_size, enc_size):
        super().__init__()
        self.norm_hidden = nn.LayerNorm(hidden_size)
        self.norm_memory = nn.LayerNorm(enc_size)
        self.attn = nn.Linear(hidden_size, enc_size)

        self.score_mask_value = -float('inf')

    def forward(self, attn_hidden, memory, mask):
        # attn_hidden : [B, chn.encoder]
        # memory : [B, chn.seg_enc, T]
        attn_hidden = self.norm_hidden(attn_hidden)
        memory = memory.transpose(1, 2)  # [B, T, chn.seg_enc]
        processed_memory = self.norm_memory(memory)
        alignment = torch.bmm(self.attn(attn_hidden).unsqueeze(1), processed_memory.transpose(1, 2))
        # [B, 1, chn.seg_enc] @ [B, chn.seg_enc, T] -> [B, 1, T]
        alignment = alignment.squeeze(1)  # [B, T]

        if mask is not None:
            alignment.data.masked_fill_(mask, self.score_mask_value)

        attn_weights = F.softmax(alignment, dim=1)  # [B, T]
        context = torch.bmm(attn_weights.unsqueeze(1), memory)
        # [B, 1, T] @ [B, T, chn.seg_enc] -> [B, 1, chn.seg_enc]

        return context, attn_weights


class VAE(nn.Module):
    def __init__(self, in_channels, latent_size):
        super().__init__()
        self.conv_layers = nn.Sequential(
            nn.Conv1d(in_channels=in_channels, out_channels=64, kernel_size=3, padding=1),
            nn.ReLU(),
        )

        self.mu = nn.Linear(64, latent_size)
        self.log_var = nn.Linear(64, latent_size)

    def forward(self, k, c):
        x = torch.cat((k, c), dim=-1)  # [B, 1, chn.seg_enc + 2 * chn.latent]
        convolved_x = self.conv_layers(x)
        # [B, 64, chn.seg_enc + 2 * chn.latent]

        # global average pooling
        avg_pool = torch.mean(convolved_x, 2)  # [B, 64]

        mu = self.mu(avg_pool)
        log_var = self.log_var(avg_pool)

        std = log_var.mul(0.5).exp_()
        eps = torch.randn_like(mu)

        z = eps * std + mu

        return mu, log_var, z


class FVAE(nn.Module):
    def __init__(self, in_dim, channels, kernel_size, text_enc_size, latent_dim):
        super().__init__()
        self.latent_dim = latent_dim

        padding = (kernel_size - 1) // 2
        self.cnn = nn.Sequential(
            nn.Conv1d(in_dim, channels, kernel_size=kernel_size, padding=padding),
            nn.ReLU(),
            )

        self.lstm = nn.LSTM(channels, channels // 2, 2, batch_first=True, bidirectional=True)

        self.attention = Attention(text_enc_size, channels)
        self.vae = VAE(1, latent_dim)
        self.proj = nn.Linear(latent_dim, 2 * latent_dim)

    def forward(self, mel_spec, text_encoding, mask, output_lengths):
        # mel_spec : [B, M, T]
        # text_encoding : [B, N, chn.encoder]
        x = self.cnn(mel_spec)
        x = x.transpose(1, 2)

        output_lengths = output_lengths.cpu().numpy()
        x = nn.utils.rnn.pack_padded_sequence(
            x, output_lengths, batch_first=True, enforce_sorted=False)

        self.lstm.flatten_parameters()
        x, _ = self.lstm(x)
        x, _ = nn.utils.rnn.pad_packed_sequence(
            x, batch_first=True)

        x = x.transpose(1, 2)
        # x : [B, chn.seg_enc, T]

        mu_list, log_var_list, latent, alignments = [], [], [], []
        k = torch.zeros(text_encoding.size(0), 1, 2 * self.latent_dim, device=text_encoding.device)

        while len(latent) < text_encoding.size(1):
            input = text_encoding[:, len(latent), :]
            c, prev_attn = self.attention(input, x, mask)  # [B, 1, chn.seg_enc], [B, T]
            mu, log_var, z = self.vae(k, c)  # [B, chn.latent]
            k += self.proj(z.unsqueeze(1))  # [B, 1, 2 * hp.chn.latent]

            mu_list.append(mu.squeeze(1))
            log_var_list.append(log_var.squeeze(1))
            latent.append(z)
            alignments.append(prev_attn)
        latent = torch.stack(latent, dim=1)
        alignments = torch.stack(alignments, dim=0).transpose(0, 1).contiguous()
        # align: [T_enc, B, T_dec] -> [B, T_enc, T_dec]

        return mu_list, log_var_list, latent, alignments

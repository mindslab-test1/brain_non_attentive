FROM nvcr.io/nvidia/pytorch:19.06-py3

RUN python3 -m pip uninstall -y apex && \
    python3 -m pip --no-cache-dir install --upgrade \
        torch==1.5.1+cu101 \
        torchvision==0.6.1+cu101 \
        -f https://download.pytorch.org/whl/torch_stable.html && \
    /opt/conda/bin/conda install openblas!=0.3.6 && \
    cd /tmp && \
    git clone https://github.com/NVIDIA/apex && \
    cd apex && \
    python3 -m pip install -v --no-cache-dir \
        --global-option="--pyprof" \
        --global-option="--cpp_ext" \
        --global-option="--cuda_ext" \
        --global-option="--bnp" \
        --global-option="--xentropy" \
        --global-option="--deprecated_fused_adam" ./ && \
# ==================================================================
# config & cleanup
# ------------------------------------------------------------------
    ldconfig && \
    apt-get clean && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* /workspace/*

RUN python3 -m pip --no-cache-dir install --upgrade \
        tensorboard==2.0.0 \
        omegaconf==2.0.0 \
        pytorch_lightning==0.7.6 \
        gpustat==0.6.0 \
        grpcio==1.13.0 \
        grpcio-tools==1.13.0 \
        protobuf==3.6.0 \
        xpinyin==0.5.7 \
        tgt==1.4.4 \
	&& \
apt update && \
apt install -y \
    tmux \
    htop \
    ncdu && \
apt clean && \
apt autoremove && \
rm -rf /var/lib/apt/lists/* /tmp/* && \
mkdir /root/brain_dca
COPY . /root/brain_dca
RUN cd /root/brain_dca/ && \
    python3 -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. tts.proto
